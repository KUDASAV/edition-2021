# team lingua

Team lingua programming competion 2021

## Getting started
system requirements
* Node JS (https://nodejs.org/en/)

install javascript dependancies:
```bash
$ npm install
```

running the webserver:
```bash
$ node index
```

run database migrations
```bash
$ cd app && npx sequelize-cli db:migrate
```

live url
https://project-alpha-gilt.vercel.app