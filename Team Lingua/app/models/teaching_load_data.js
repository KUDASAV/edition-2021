const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    users = sequelize.define(
        "teaching_load_data",
        {
            id: {
                autoIncrement: true,
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
            },
            user_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            year: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            students_enrolled: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            semester: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            course: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            course_level: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            comments: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            }
        },
        {
            sequelize,
            tableName: "teaching_load_data",
            timestamps: false,
            indexes: [
                {
                    name: "PRIMARY",
                    unique: true,
                    using: "BTREE",
                    fields: [{ name: "id" }],
                },
            ],
        }
    );

    return users
};
