const Sequelize = require("sequelize");

module.exports = function (sequelize, DataTypes) {
    users = sequelize.define(
        "community_engagement_data",
        {
            id: {
                autoIncrement: true,
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
            },
            user_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            outreach_activity: {
                type: DataTypes.STRING(255),
                allowNull: false,
            },
            comments: {
                type: DataTypes.STRING(255),
                allowNull: false,
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: Sequelize.Sequelize.literal("CURRENT_TIMESTAMP"),
            }
        },
        {
            sequelize,
            tableName: "community_engagement_data",
            timestamps: false,
            indexes: [
                {
                    name: "PRIMARY",
                    unique: true,
                    using: "BTREE",
                    fields: [{ name: "id" }],
                },
            ],
        }
    );

    return users;
};
