const express = require("express");
const router = express.Router();
const passport = require("passport");
const Models = require("../models");

//send our home page
router.all("/", async function (req, res) {
    session = req.session;
    console.log("session", session);

    if (session.user) {
        user = await Models.users.findOne({
            where: { email: req.session.user.email },
        });

        if(user){
            if (user.dataValues.role == "Staff") {
                return res.redirect("/workload");
            }
    
            return res.redirect("/users");
        }
    }

    res.render("login.html", {
        body_class: "login_page",
        page_title: "Workload Generator",
    });
});

// dashboard page
router.all("/dashboard", async function (req, res) {
    // get the user associated to an email
    console.log(req.session.user);
    user = await Models.users.findOne({
        where: { email: req.session.user.email },
    });

    console.log(user.dataValues);

    if (user.dataValues.role == "Staff") {
        return res.render("staff_home.html", {
            body_class: "login_page",
            session: req.session,
            page_title: "Workload Generator",
        });
    }

    //res.redirect("/workload");
    res.render("home.html", {
        body_class: "login_page",
        session: req.session,
        page_title: "Workload Generator",
    });
});

// add staff member page
router.all("/add-staff", async function (req, res) {
    res.render("create_user.html", {
        body_class: "login_page",
        session: req.session,
        page_title: "Add Staff",
    });
});

// display user details
router.all("/user/:user_id/courses", async function (req, res) {
    res.render("user_courses.html", {
        body_class: "login_page",
        session: req.session,
        page_title: "Add Staff",
    });
});

// display user details
router.all("/user/:user_id/allocate-course", async function (req, res) {
    res.render("allocate_course.html", {
        body_class: "login_page",
        session: req.session,
        page_title: "Add Staff",
    });
});

// display workload sheets
router.all("/workload/:id/view/", async function (req, res) {
    res.render("workload_sheet.html", {
        body_class: "login_page",
        session: req.session,
        page_title: "Add Staff",
    });
});

// authentication urls
router.get(
    "/auth/google",
    passport.authenticate("google", { scope: ["profile", "email"] })
);

router.get(
    "/auth/google/callback",
    passport.authenticate("google", { failureRedirect: "/error" }),
    async function (req, res) {
        user = await Models.users.findOne({
            where: { email: req.user._json.email },
        });

        if (user) {
            // setup user session and redirect to dashboard
            session = req.session;
            session.user = req.user._json;
            session.user_details = user

            // set cookies to expire after one week
            var hour = 3600000;
            req.session.cookie.maxAge = 14 * 24 * hour;
        }

        return res.redirect("/");
    }
);

router.use("/courses", require("./courses"));
router.use("/users", require("./users"));
router.use("/workload", require("./workload"));

module.exports = router;
