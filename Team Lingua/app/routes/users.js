const express = require('express')
const router = express.Router();
const Models = require("../models");
const user_courses = require('../models/user_courses');
const { Op } = require("sequelize");

// users
router.get('/', async function (req, res) {
    // get all users in the db
    let users = await Models.users.findAll();
    console.log(users)

    return res.render('home.html', {
        body_class: "login_page",
        session: req.session,
        users: users,
        page_title: "Add Staff",
    })
})

// user account creation form
router.get('/add', async function (req, res) {
    return res.render('create_user.html', {
        body_class: "login_page",
        session: req.session,
        page_title: "Add User",
    })
})

// create user accounts
router.post('/add', async function (req, res) {
    try{
        await Models.users.create(req.body);
        return res.redirect("/users")
    }catch(e){
        console.log(e)
        return res.redirect("/users/add")
    }
})

// return user details
router.get('/:id', async function (req, res) {
    console.log("USER DETAILS")
    var user_id = req.params.id

    // get a user's account by thier user id
    user_details = await Models.users.findOne({
        where: { id: user_id },
    });

    return res.render('user_details.html', {
        body_class: "login_page",
        user_id: user_id,
        details: user_details,
        session: req.session,
        page_title: "Add Staff",
    })
})

// return user details
router.get('/:id/permissions', async function (req, res) {
    var user_id = req.params.id

    // get a user's account by thier user id
    user_details = await Models.users.findOne({
        where: { id: user_id },
    });

    return res.render('user_permissions.html', {
        body_class: "login_page",
        user_id: user_id,
        details: user_details,
        session: req.session,
        page_title: "Add Staff",
    })
})

// resturn courses allocated to the user
router.get('/:id/courses', async function (req, res) {
    var user_id = req.params.id

    // get a user's account by thier user id
    allocated_courses = await Models.user_courses.findAll({
        where: { user_id: user_id },
    });

    // get a list ou all course ids
    course_ids = []
    allocated_courses.forEach(item => {
        course_ids.push(item.course_id)
    });

    courses_details = await Models.courses.findAll({
        where: { 
            id: {[Op.in]: course_ids}
        },
    });

    return res.render('user_courses.html', {
        body_class: "login_page",
        user_id: user_id,
        session: req.session,
        courses_details: courses_details,
        page_title: "Add Staff",
    })
})

// returun corse allocation form
router.get('/:id/courses/allocate', async function (req, res) {
    // get available courses
    courses = await Models.courses.findAll();

    return res.render('allocate_course.html', {
        body_class: "login_page",
        courses: courses,
        session: req.session,
        page_title: "Add Staff",
    })
})

// allocate a course to a user
router.post('/:id/courses/allocate', async function (req, res) {
    var user_id = req.params.id
    try{
        //await Models.users.create(req.body);
        return res.redirect("/users/"+user_id+"/courses")
    }catch(e){
        console.log(e)
        return res.redirect("/users/"+user_id+"/courses")
    }
})



module.exports = router